<?php
	/**
		description:消息基类，所有实现的消息均需继承自MSG类
		author:王海阳
		time:2013/4/18
	*/
	require_once("baseIO.interface.php");
	
	class MSG implements IMSG
	{
		protected $msgCreateTime;
		protected $msgType;
		
		public function __construct($time,$type)
		{
			$this->msgCreateTime = $time;
			$this->msgType = $type;
		}
		
		public function getMSGCreateTime()
		{
			return $this->msgCreateTime;
		}
		
		public function getMSGType()
		{
			return $this->msgType;
		}
	}