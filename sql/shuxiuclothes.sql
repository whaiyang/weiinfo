-- phpMyAdmin SQL Dump
-- version 4.1.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 2014-04-17 17:51:44
-- 服务器版本： 5.6.11
-- PHP Version: 5.5.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shuxiuclothes`
--

-- --------------------------------------------------------

--
-- 表的结构 `sx_category`
--

CREATE TABLE IF NOT EXISTS `sx_category` (
  `cid` int(10) NOT NULL AUTO_INCREMENT,
  `pid` int(10) DEFAULT '0' COMMENT '父栏目id',
  `sequence` int(10) NOT NULL DEFAULT '0',
  `show` int(1) NOT NULL DEFAULT '1',
  `type` int(5) NOT NULL COMMENT '0:列表栏目；1：单页栏目',
  `name` varchar(250) NOT NULL,
  `tplname` varchar(100) NOT NULL DEFAULT 'list.html',
  PRIMARY KEY (`cid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- 转存表中的数据 `sx_category`
--

INSERT INTO `sx_category` (`cid`, `pid`, `sequence`, `show`, `type`, `name`, `tplname`) VALUES
(2, 0, 0, 1, 0, 'TEST', 'list.html'),
(3, 0, 0, 1, 1, 'TEST2', 'content2.html'),
(4, 0, 0, 1, 0, 'TEST2', 'list.html');

-- --------------------------------------------------------

--
-- 表的结构 `sx_content`
--

CREATE TABLE IF NOT EXISTS `sx_content` (
  `aid` int(10) NOT NULL AUTO_INCREMENT,
  `cid` int(10) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `content` text,
  `description` varchar(250) DEFAULT NULL,
  `show` int(1) NOT NULL DEFAULT '1',
  `updatetime` int(10) DEFAULT NULL,
  `inputtime` int(10) DEFAULT NULL,
  `sequence` int(10) NOT NULL DEFAULT '0',
  `tplname` varchar(100) NOT NULL DEFAULT 'content.html',
  `views` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`aid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- 转存表中的数据 `sx_content`
--

INSERT INTO `sx_content` (`aid`, `cid`, `title`, `content`, `description`, `show`, `updatetime`, `inputtime`, `sequence`, `tplname`, `views`) VALUES
(2, 2, 'TEST', '你好11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111\n11111111111111111111111111111111', NULL, 1, 1390650946, 1390650857, 0, 'content.html', 20),
(3, 3, 'SinglePage', 'asasasasasasasaa', NULL, 1, 1390653947, 1390653947, 0, 'content.html', 2),
(4, 2, 'TE', 'aaaaa', NULL, 1, 1390656015, 1390656015, 0, 'content.html', 0),
(5, 2, 'TE', 'aaaaa', NULL, 1, 1390656035, 1390656035, 0, 'content.html', 0),
(6, 2, 'TE2', 'aaaaa', NULL, 1, 1390661170, 1390661170, 0, 'content.html', 1),
(7, 2, 'TE3', '', NULL, 1, 1390661196, 1390661196, 0, 'content.html', 0),
(8, 2, 'TE3', '', NULL, 1, 1390661249, 1390661249, 0, 'content.html', 0),
(9, 2, 'TE3', '', NULL, 1, 1390661285, 1390661285, 0, 'content.html', 0),
(10, 2, 'TE3', '', NULL, 1, 1390663670, 1390663670, 0, 'content.html', 0),
(11, 2, 'TE3', '', NULL, 1, 1390664706, 1390664706, 0, 'content.html', 0),
(13, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/ueditor/php/../../Public/upload/20140126/1390672334236115.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p&gt;aaa&lt;/p&gt;', NULL, 1, 1390675504, 1390675504, 0, 'content.html', 40),
(14, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/ueditor/php/../../public/upload/20140126/1390672334236115.jpg\\&quot;/&gt;&lt;/p&gt;', NULL, 1, 1390678193, 1390678193, 0, 'content.html', 1),
(15, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/ueditor/php/../../public/upload/20140126/1390678561112734.jpg\\&quot; title=\\&quot;KT板模版（创新创业展）.jpg\\&quot;/&gt;&lt;/p&gt;', NULL, 1, 1390678573, 1390678573, 0, 'content.html', 2),
(16, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/ueditor/php/../../public/upload/20140126/1390678561112734.jpg\\&quot; title=\\&quot;KT板模版（创新创业展）.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p style=\\&quot;text-align: center', NULL, 1, 1390678634, 1390678634, 0, 'content.html', 3),
(17, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/ueditor/php/../../public/upload/20140126/1390678561112734.jpg\\&quot; title=\\&quot;KT板模版（创新创业展）.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p style=\\&quot;text-align: center', NULL, 1, 1390678759, 1390678759, 0, 'content.html', 4),
(18, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/ueditor/php/../../public/upload/20140126/1390678561112734.jpg\\&quot; title=\\&quot;KT板模版（创新创业展）.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p style=\\&quot;text-align: center', NULL, 0, 1390678861, 1390678861, 1, 'content.html', 0),
(19, 2, 'UETest', '&lt;p&gt;&lt;img src=\\&quot;http:', NULL, 1, 1390679089, 1390679089, 0, 'content.html', 0),
(20, 2, 'UETest', '&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/public/upload/20140126/1390678561112734.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal; text-align: center;\\&quot;&gt;&lt;span style=\\&quot;font-size: 36px; color: rgb(255, 0, 0);\\&quot;&gt;测试&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', NULL, 1, 1390679661, 1390679661, 0, 'content.html', 3),
(21, 2, 'UETest', '&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;测试测试测试&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/public/upload/20140126/1390678561112734.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal; text-align: center;\\&quot;&gt;&lt;span style=\\&quot;font-size: 36px; color: rgb(255, 0, 0);\\&quot;&gt;测试&lt;/span&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', NULL, 1, 1390681549, 1390681549, 0, 'content.html', 1),
(22, 2, 'UETest', '&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;测试测试测试&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/public/upload/20140126/1390678561112734.jpg\\&quot;/&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal; text-align: center;\\&quot;&gt;&lt;span style=\\&quot;font-size: 36px; color: rgb(255, 0, 0);\\&quot;&gt;测试&lt;/span&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;', '&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;测试测试测试&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;br/&gt;&lt;/p&gt;&lt;p style=\\&quot;white-space: normal;\\&quot;&gt;&lt;img src=\\&quot;http://localhost/shuxiu/Tpl/SPage/admin/publ', 1, 1390681651, 1390681651, 0, 'content.html', 1),
(23, 2, 'UETest', '&lt;p&gt;&lt;strong&gt;Flash视频测试&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;embed type=\\&quot;application/x-shockwave-flash\\&quot; class=\\&quot;edui-faked-video\\&quot; pluginspage=\\&quot;http://www.macromedia.com/go/getflashplayer\\&quot; src=\\&quot;http://player.youku.com/player.php/Type/Folder/Fid/21788418/Ob/1/sid/XNjY1NDU3MjU2/v.swf\\&quot; width=\\&quot;420\\&quot; height=\\&quot;280\\&quot; wmode=\\&quot;transparent\\&quot; play=\\&quot;true\\&quot; loop=\\&quot;false\\&quot; menu=\\&quot;false\\&quot; allowscriptaccess=\\&quot;never\\&quot; allowfullscreen=\\&quot;true\\&quot;/&gt;&lt;/p&gt;', '&lt;p&gt;&lt;strong&gt;Flash视频测试&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;embed type=\\&quot;application/x-shockwave-flash\\&quot; class=\\&quot;edui-faked-video\\&quot; pluginspage=\\&quot;http://www.macromedia.com/go/getflashplayer\\&quot; src=\\&quot;http://', 1, 1390682155, 1390682155, 0, 'content.html', 3);

-- --------------------------------------------------------

--
-- 表的结构 `sx_user`
--

CREATE TABLE IF NOT EXISTS `sx_user` (
  `uid` bigint(15) NOT NULL AUTO_INCREMENT,
  `real_name` varchar(30) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(32) NOT NULL,
  `regi_time` int(10) NOT NULL,
  `checked` bit(1) NOT NULL,
  `group` int(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- 转存表中的数据 `sx_user`
--

INSERT INTO `sx_user` (`uid`, `real_name`, `phone`, `email`, `password`, `regi_time`, `checked`, `group`) VALUES
(7, '王海阳', '18782246522', '529570509@qq.com', '202cb962ac59075b964b07152d234b70', 1390638395, b'1', 0),
(14, 'www', '18782256522', '222@qq.com', '099b3b060154898840f0ebdfb46ec78f', 1395138523, b'0', 0),
(15, 'whywhy', '1212121212', 'www@qq.com', 'e10adc3949ba59abbe56e057f20f883e', 1395139992, b'0', 0),
(16, 'qq', '1111111', 'qq@qq.com', '099b3b060154898840f0ebdfb46ec78f', 1395140067, b'1', 0),
(17, 'qqq', '1111111', 'qqq@qq.com', 'b2ca678b4c936f905fb82f2733f5297f', 1395140930, b'1', 0);

-- --------------------------------------------------------

--
-- 表的结构 `sx_user_extra`
--

CREATE TABLE IF NOT EXISTS `sx_user_extra` (
  `uid` bigint(15) NOT NULL,
  `PWId` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `sx_user_extra`
--

INSERT INTO `sx_user_extra` (`uid`, `PWId`) VALUES
(2, '1390293525');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
