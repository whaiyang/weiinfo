<?php
	class RssReader
	{
		private $rssObj = null;
		private $rssObjs = array();
		
		//和获取单个RSS
		public function get($url,$charsetF=null,$charsetT="UTF-8")
		{
			$rss = file_get_contents($url);
			if($charsetF!=null)
				$rss = mb_convert_encoding($rss,"utf8","gb2312");//iconv($charsetF,$charsetT,$rss);//
			$this->rssObj = simplexml_load_string($rss, 'SimpleXMLElement', LIBXML_NOCDATA);
			return $this->rssObj;
		}
		
		//获取一组rss
		public function getFArr($urls,$charsetF=null,$charsetT="UTF-8")
		{
			foreach($urls as $url)
			{
				$rss = file_get_contents($url);
				if($charsetF!=null)
					$rss = mb_convert_encoding($rss,$charsetT,$charsetF);
				$this->rssObjs[] = simplexml_load_string($rss, 'SimpleXMLElement', LIBXML_NOCDATA);
			}
			return $this->rssObjs;
		}
		
		public static function getImage($item)
		{
			$str = $item->description;
			//$arr = array();
			//$img = array();
			//$patternImg = '<img/s+[^>]*/s*src/s*=/s*([']?)(?<url>/S+)'?[^>]*>';
			/*$pattern = "/<img\ssrc=\"(http:\/\/.+\.(jpg|gif|bmp|bnp))\">/i";
			preg_match($pattern,$str,$arr);
			preg_match($pattern,$arr[0],$img);*/
			preg_match("/<img([^>]*)\s*src=('|\")([^'\"]+)('|\")/", $str,$matches);
			$str = $matches[0];
			preg_match('/[a-zA-Z]+://[^\s]*/',$str,$matches);
			return $matches[0];
		}
		
		//从已获取的一个rss中选择
		public function getMaxFObj($offset,$count)
		{
			$items = array();
			if($this->rssObj!=null)
			{
				$mlen = count($this->rssObj->channel->item);
				for($i=0;$i<$count&&$offset<$mlen;$i++,$offset++)
				{
					$items[] = $this->rssObj->channel->item[$offset];
				}
			}
			return $items;
		}
		
		//从已获取的一组rss中选择
		public function getMaxFArr($offset,$count)
		{
			$items = array();
			if(count($this->rssObjs)>0)
			{
				foreach($this->rssObjs as $rss)
				{
					$items[] = array();
					$curindex = count($items)-1;
					$mlen = count($rss->channel->item);
					for($i=0;$i<$count&&$offset<$mlen;$i++,$offset++)
					{
						$items[$curindex][] = $rss->channel->item[$offset];
					}
				}
			}
			return $items;
		}
	}