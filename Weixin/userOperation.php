<?php
	require_once("music/MusicSearch.php");	
	
	function onSubscribe($msg,$db,$forceSend=false)
	{
		$event = "";
		if($msg->getMSGType()=="event")
			$event = $msg->getEvent();
		if($event=="subscribe"||$forceSend)
		{
			$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_TEXT);
			$sendMsg->setContent("志鹏俊马科技有限公司");
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}

	/**
		微信菜单点击事件
	*/
	function onClick($msg,$db)
	{
		$sendMsg = null;
		switch ($msg->getEventKey()) {
			case '0_1':
				$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
				$sendMsg->addItem(array('title'=>'幸运大转盘','description'=>'','picUrl'=>getPicUrl("roulette_cover.jpg"),'url'=>setController("roulette","index",$msg->getFUserN())));
				break;
			case '1_2':
				$sendMsg = weiMSGBuilder::build($msg,weiMSGBuilder::$MSG_NEWS);
				$sendMsg->addItem(array('title'=>'预约看房','description'=>'','picUrl'=>getPicUrl("reserve.jpg"),'url'=>setController("reserve","index",$msg->getFUserN())));
				break;
		}
		if(!empty($sendMsg))
		{
			$sendMsg->changeTarget();
			return $sendMsg;
		}
		else
		{
			return $msg;
		}
	}
	