var reserve = {
	// attributions of data
	datebox : Array()
	,

	// attributions of function
	// actions of the pages
	// -reserve page functions
	findDatebox : function() {
		$("input:text").each(function(){
			if ($(this).attr("date") == "1") {
				reserve.datebox.push($(this));
			}
		});
			console.log("find true");
		return false;
	}
	,
	dateboxIni : function() {
		var i;
		for (i in reserve.datebox) {
			$(reserve.datebox[i]).datepicker();
		}
		console.log("init true");
		return false;
	}
	,
	// -DC libaray page functions
	switchLib : function() {
		$(".lib_switch h5").click(function(){
			var pa = $(this).parent();
			var iBody = pa.find(".lib_body");
			var iHead = pa.find("span");
			var before = $(".lib_switch").not(this).find(".lib_body:visible:first").parent();

			if (pa.find(".lib_body").is(":visible")) {
				iBody.slideUp(150);
				iHead.removeClass("active");
				return false;
			}

			iBody.slideDown(150);
			iHead.addClass("active");
			if (before.length > 0){
				before.find(".lib_body").slideUp(150);
				before.find("span").removeClass("active");
			}
		});
		return false;
	}
	,
	switchSrc : function() {
		$(".lib_body").find("li").click(function(){
			var target = $(this).attr("data-target");
			$("#lib_page").find("iframe").attr("src", target);
		});
		return false;
	}
	,

	// init functions
	initReserve : function() {
		// find the datepicker boxes
		reserve.findDatebox();
		// datepicker
		reserve.dateboxIni();
	}
	,
	initLib : function() {
		reserve.switchLib();
		reserve.switchSrc();
	}
	// ,
};


var menu_p = {
	// i : lenght of top ones
	// ,
	// 添加一二级选项的容器
	dl : '<dl class="inner_menu"></dl>'
	,
	// 一级选项容器
	dt : '<dt class="inner_menu_item" id="temperID" data-son="p" data-type="null" data-value=""><i class="icon_inner_menu_switch"></i><a href="javascript:void(0);" class="inner_menu_link"><strong></strong></a><span class="menu_opr"><a href="javascript:void(0);" class="icon14_common add_gray jsAddBt">添加</a><a href="javascript:void(0);" class="icon14_common  edit_gray jsEditBt">编辑</a><a href="javascript:void(0);" class="icon14_common del_gray jsDelBt">删除</a><a href="javascript:void(0);" class="icon14_common sort_gray jsOrderBt" style="display:none">排序</a></span></dt>'
	,
	// 二级选项容器
	dd : '<dd class="inner_menu_item jslevel2" id="temperID" data-son="s" data-type="null" data-value=""><i class="icon_dot">●</i><a href="javascript:void(0);" class="inner_menu_link"><strong></strong></a><span class="menu_opr"><a href="javascript:void(0);" class="icon14_common edit_gray jsSubEditBt">编辑</a><a href="javascript:void(0);" class="icon14_common del_gray jsSubDelBt">删除</a><a href="javascript:void(0);" class="icon14_common sort_gray jsOrderBt" style="display:none">排序</a></span></dd>'
	,
	// pop框提交名称信息的from内容 <<暂停该功能，升级时可用 tristan
	fName : '<form id="popupForm_" method="post" class="form" onsubmit="return false;" novalidate="novalidate"><div class="frm_control_group"><label class="frm_label">菜单名称名字不多于4个汉字或8个字母:</label><span class="frm_input_box"><input type="text" class="frm_input" name="popInput" value="" style="width:380px;"><input style="display:none"></span></div><div class="js_verifycode"></div></form>'
	,
	// 为新加一级选项添加名字 <<暂停该功能，升级时可用 tristan
	addName : function() {
		$("#mask").attr("class", "mask");
		$(".simple_dialog_content:first").html("");
		$(".simple_dialog_content:first").html(menu_p.fName);
		return false;
	}
	,
	// 添加一级选项
	addBt : function(val){
		var id = "menu_" + menu_p.i;
		$("#menuList").append(menu_p.dl);
		$("#menuList").find(".inner_menu:last").append(menu_p.dt);
		// 绑定事件
		$("#temperID").find(".jsEditBt").click(function(){
			$("#mask input[type=text]").val("");
			$("#mask").attr("class", "mask");
			temp = $(this);
		});
		$("#temperID").find(".jsDelBt").click(function(){
			var box =$(this).parent().parent();
			data.droptopmenu(box.attr("data-num"));
			box.parent().remove();
			menu_p.i--;
			return false;
		});
		$("#temperID").find(".jsAddBt").click(function(){
			var val = "未命名";
			temp = $(this);
			menu_p.addDd(val);
			return false;
		});
		menu_p.bindActive($("#temperID"));
		$("#temperID").find("strong").html(val);
		$("#temperID").attr("num", menu_p.i);
		$("#temperID").click();
		menu_p.order();
		data.addtopmenu($("#temperID").attr("data-num"), "未命名");
		$("#temperID").attr("id", id);
		menu_p.i++;
		return false;
	}
	,
	// 添加二级选项
	addDd : function(val) {
		var box = temp.parent().parent().parent();
		var j = box.find("dd").length;

		if (box.find("dt").attr("data-type") != "null"){
			$(".action_content:visible").each(function(){
				$(this).css({
					"display" : "none"
				});
			});
			$("#addSub").css({
				"display" : "block"
			});
			return false;
		}

		box.find("dt").attr("data-son","p-s");
		box.append(menu_p.dd);
		// 绑定事件
		$("#temperID").find(".jsSubEditBt").click(function(){
			$("#mask input[type=text]").val("");
			$("#mask").attr("class", "mask");
			temp = $(this);
		});
		$("#temperID").find(".jsSubDelBt").click(function(){
			var box = $(this).parent().parent();
			var i = box.parent().find("dd").length;
			if (!(i-1)) {
				box.parent().find("dt").attr("data-son", "p");
			}
			box.remove();
			return false;
		});
		menu_p.bindActive($("#temperID"));
		$("#temperID").find("strong").html(val);
		$("#temperID").click();
		menu_p.order();
		data.addtopmenu($("#temperID").attr("data-num1"), "未命名");
		$("#temperID").attr("id", "");
		return false;
	}
	,
	// 检查字符串长度
	widthCheck : function(str, maxLen) {
		var w = 0;  
		var tempCount = 0; 
		for (var i=0; i<str.length; i++) {  
		   var c = str[i];
		   //英文及数字加1  
		   if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f) || ("a"<=c && c<="z") || ("A"<=c && c<="Z")) {  
		    w++;  
		   }
		   //汉字加2  
		   else {
		    w+=2;  
		   }
		   if (w > maxLen) {  
			   return false;
		   }    
		}
		return true;
	}
	,
	//为选项绑定点击事件
	bindActive : function(box) {
		var container = $(".inner_menu_box");
		box.click(function(){
			container.find(".selected").removeClass("selected");
			$(this).addClass("selected");

			$(".action_content:visible").each(function(){
				$(this).css({
					"display" : "none"
				});
			});

			if (box.get(0).tagName == "DT") {
				if (box.attr("data-son") == "p-s") {
					// 已有子元素不能选择属性
					$("#alreadySon").css({
						"display" : "block"
					});
				}
				else {
					// 没有子元素
					switch (box.attr("data-type")) {
						case "null":
						$("#index").css({
							"display" : "block"
						});
						break;

						case "mail":
						$("#edit").find("input:first").val(box.attr("data-value"));
						$("#edit").css({
							"display" : "block"
						});
						break;

						case "url":
						$("#url").find("input:first").val(box.attr("data-value"));
						$("#url").css({
							"display" : "block"
						});
						break;

						default:
						console.log("data-type error!");
						break;
					} 
				}
			}
			else {
				// 子元素
				switch (box.attr("data-type")) {
					case "null":
					$("#index").css({
						"display" : "block"
					});
					break;

					case "mail":
					$("#edit").find("input:first").val(box.attr("data-value"));
					$("#edit").css({
						"display" : "block"
					});
					break;

					case "url":
					$("#url").find("input:first").val(box.attr("data-value"));
					$("#url").css({
						"display" : "block"
					});
					break;

					default:
					console.log("data-type error!");
					break;
				} 
			}
		});
		return false;
	}
	,
	iniRight : function() {
		if (!$(".inner_menu_box").find("dl").length) {
			$("#none").attr("style","display:block;");
		}
		return false;
	}
	,
	iniChoice : function() {
		$("#sendMsg").click(function(){
			var box = $(".inner_menu_box").find(".selected");
			$("#edit").find("input:first").val("");
			box.attr("data-type", "mail");
			$("#index").css({
				"display" : "none"
			});
			$("#edit").css({
				"display" : "block"
			});
		});

		$("#goPage").click(function(){
			var box = $(".inner_menu_box").find(".selected");
			$("#url").find("input:first").val("");
			box.attr("data-type", "url");
			$("#index").css({
				"display" : "none"
			});
			$("#url").css({
				"display" : "block"
			});
		});
		return false;
	}
	,
	toolBack : function(box) {
		var bar = $(".inner_menu_box").find(".selected");
		$(".action_content:visible").each(function(){
			$(this).css({
			"display" : "none"
			});
		});
		if (box.attr("id") == "addSub") {
			bar.click();
		}
		else {
			bar.attr("data-type", "null");
			bar.click();
		}
		return false;
	}
	,
	toolSub : function(box) {
		var bar = $(".inner_menu_box").find(".selected");
		if (box.attr("id") == "addSub") {
			$(".inner_menu_box .selected").attr({
				"data-type" : "null",
				"data-son" : "p-s",
				"data-value" : ""
			});
			$(".inner_menu_box .selected").find(".jsAddBt").click();
		}
		else if (box.attr("id") == "url") {
			$(".inner_menu_box .selected").attr({
				"data-type" : "url",
				"data-value" : box.find("input:first").val()
			});
		}
		else if (box.attr("id") == "edit") {
			$(".inner_menu_box .selected").attr({
				"data-type" : "edit",
				"data-value" : box.find("input:first").val()
			});
		}
	}
	,
	order : function() {
		var i = 0, j = 0;
		$(".inner_menu_box").find("dl").each(function(){
			$(this).find("dt").attr("data-num", i);
			j = 0;
			$(this).find("dd").each(function(){
				$(this).attr("data-num1", i);
				$(this).attr("data-num2", j);
				j++;
			});
			i++;
		});
		return false;
	}
	,
	// 初始化menu页面元素
	initMenu : function(){
		data = menu.createNew();
		$("#addBt").click(function(){
			// menu_p.addName();
			var val;
			// val = $(".frm_input").val();
			val = "未命名";
			menu_p.addBt(val);
			return false;
		});
		menu_p.i = $("#menuList").find("dl").length;
		$(".pop_closed").click(function(){
			$("#mask").attr("class", "maskPre");
			return false;
		});
		$(".dialog_ft").find("input:last").click(function(){
			$("#mask").attr("class", "maskPre");
			return false;
		});
		$(".dialog_ft").find("input:first").click(function(){
			// 添加名字
			// var val;
			// val = $(".frm_input").val();
			// menu_p.addBt(val);
			// $("#mask").attr("class", "maskPre");
			// return false;
			var val = $("#mask input[type=text]").val();
			if (!menu_p.widthCheck(val, 8)){
				$("#mask input[type=text]").val("");
				$("#mask input[type=text]").attr("placeholder", "字符过长，请重新输入");
				return false;
			}
			var box = temp.parent().parent();
			menu_p.order();
			if (box.get(0).tagName == "TD") {
				data.addtopmenu(box.attr("data-num"), val);
			}
			else {
				data.addsubmenu(box.attr("data-num1"), val);
			}
			box.find("strong:first").html(val);
			$("#mask").attr("class", "maskPre");
			return false;
		});
		// 添加子选项
		$(".jsAddBt").click(function(){
			var val = "未命名";
			temp = $(this);
			menu_p.addDd(val);
			return false;
		});
		// 删除主选项
		$(".jsDelBt").click(function(){
			var box = $(this).parent().parent();
			data.droptopmenu(box.attr("data-num"));
			box.parent().remove();
			menu_p.i--;
			return false;
		});
		// 删除子选项
		$(".jsSubDelBt").click(function(){
			var box = $(this).parent().parent();
			var i = box.parent().find("dd").length;
			if (!(i-1)) {
				box.parent().find("dt").attr("data-son", "p");
			}
			data.dropsubmenu(box.attr("data-num1"), box.attr("data-num2"));
			box.remove();
			return false;
		});
		// 修改名字
		$(".jsEditBt").click(function(){
			$("#mask input[type=text]").val("");
			$("#mask").attr("class", "mask");
			temp = $(this);
		});
		$(".jsSubEditBt").click(function(){
			$("#mask input[type=text]").val("");
			$("#mask").attr("class", "mask");
			temp = $(this);
		});
		$(".inner_menu_box dt").add(".inner_menu_box dd").each(function(){
			menu_p.bindActive($(this));
		});
		// 初始化 设置动作部分
		menu_p.iniRight();
		// 设置发送信息或跳转网页
		menu_p.iniChoice();
		// tool bar click
		$(".tool_bar").find("a.btn:first").click(function(){
			var box = $(this).parent().parent();
			menu_p.toolBack(box);
		});
		$(".tool_bar").find("a.btn:last").click(function(){
			var box = $(this).parent().parent();
			menu_p.toolSub(box);
		});
	}
	,
	// ,
};


$(document).ready(function(){
	reserve.initLib();
	menu_p.initMenu();
});