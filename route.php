<?php
	@$action = $_GET['action'];
	if(isset($action))
	{
		switch($action)
		{
			case "weixin":
				require_once("Weixin/index.php");
				break;
			case "API_DOU":
				require_once("ThirdpartApi/DouBan/index.php");
				break;
			case "YOUKU":
				require_once("ThirdpartApi/Youku/index.php");
				break;
			case "USER":
				require_once("user/index.php");
				break;
			case "MOVIE":
				require_once("board/movie/index.php");
				break;
			case "BOOK":
				require_once("board/book/index.php");
				break;
			default:
				echo "No such action.";
				break;
		}
	}
	