<!DOCTYPE HTML>
<html>
<head>
<meta name="viewport" content="width=device-width; height=device-height; initial-scale=1.0; maximum-scale=1.0;">
<meta charset="utf-8">
<title>WeiQA</title>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	function getArgs() { 
		var args = {};
        var query = location.search.substring(1);
         // Get query string
		var pairs = query.split("&"); 
                    // Break at ampersand
		for(var i = 0; i < pairs.length; i++) {
            var pos = pairs[i].indexOf('=');
             // Look for "name=value"
            if (pos == -1) continue;
                    // If not found, skip
                var argname = pairs[i].substring(0,pos);// Extract the name
                var value = pairs[i].substring(pos+1);// Extract the value
                //value = decodeURIComponent(value);// Decode it, if needed
                args[argname] = value;
                        // Store as a property
        }
		return args;// Return the object 
	}
	
	function link(e)
	{
		event = e||event;
		target = event.target||event.srcElement; 
		location.href = target.getAttribute("link");
	}
</script>
<script type="text/javascript">
	var $_GET = getArgs();
	var weiid = $_GET['weiid'];
	var videoid = $_GET['videoid'];
	var apikey = $_GET['apikey'];
	//embsig
	var VERSION = 1;
	var TIMESTAMP = new Date().getTime();
	var SIGNATURE = $_GET['SIGNATURE'];
</script>
</head>
<body>
    <div id="youkuplayer"></div>
	<script type="text/javascript" src="http://player.youku.com/jsapi">
	player = new YKU.Player('youkuplayer',{
		client_id: apikey,
		vid: videoid,
		embsig: VERSION+"_"+TIMESTAMP+"_"+SIGNATURE,
		width: 480,
		height: 400,
		autoplay: true,
		show_related: true,
		events:{
			onPlayerReady: function(){ playVideo(); }
		}
	});
		
	function playVideo()
	{
		player.playVideo();
	}
	</script>
</body>
</html>
