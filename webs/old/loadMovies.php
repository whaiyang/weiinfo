﻿<?php
	@$weiid = $_GET['weiid'];
	@$movieid = $_GET['movieid'];
	@$keyword = $_GET['keyword'];
	@$count = intval($_GET['count']);
	@$index = intval($_GET['index']);
	if(isset($keyword))
	{
		if(isset($movieid))
		{
			$movie = Dou_searchMovie($movieid);
			$title = Dou_Movie::getTitle($movie);
			$image = urldecode($_GET['picUrl']);
			$type = Dou_Movie::getType($movie);
			$type = implode(" ",$type);
			$lang = implode(" ",Dou_Movie::getLang($movie));//语言
			$avgScore = Dou_Movie::getAvgScore($movie);
			@$authors = Dou_Movie::getAuthor($movie);//编剧
			@$author = implode(" ",$authors);
			@$directors = Dou_Movie::getDirector($movie);//导演
			@$director = implode(" ",$directors);
			$country = Dou_Movie::getCountry($movie);
			$country = implode(" ",$country);
			$pubdate = implode(" ",Dou_Movie::getPubdate($movie));
			@$duration = implode(" ",Dou_Movie::getDuration($movie));//时长
			$desc = Dou_Movie::getSummary($movie);
			echo "<div class='blog'><h2 style='font-size:22px;'>$title</h2><br/><img width=297 height=444 src=$image></img><br/><h4 style='font-size:22px;'>类型：</h4>&nbsp;&nbsp;$type<br/><h4 style='font-size:22px;'>导演：</h4>&nbsp;&nbsp;$director<br/><h4 style='font-size:22px;'>编剧：</h4>&nbsp;&nbsp;$author<br/><h4 style='font-size:22px;'>国家：</h4>&nbsp;&nbsp;$country<br/><h4 style='font-size:22px;'>语言：</h4>&nbsp;&nbsp;$lang<br/><h4 style='font-size:22px;'>得分：</h4>&nbsp;&nbsp;$avgScore<br/><h4 style='font-size:22px;'>上映日期：</h4>&nbsp;&nbsp;$pubdate<br/><h4 style='font-size:22px;'>时长：</h4>&nbsp;&nbsp;$duration<br/><h4 style='font-size:22px;'>简介：</h4>&nbsp;&nbsp;$desc</div>";
		}
		else
		{
			$count = 10;
			$movies = Dou_searchMovies_list($keyword,"",$index,$count);
			foreach($movies->subjects as $movie)
			{
				if(!empty($movie))
				{
					$title = Dou_Movie::getTitle($movie);
					$image = Dou_Movie::getImages($movie);
					$image = $image["large"];
					$id = $movie->id;
					$url = setUrl(array('weiid'=>$weiid,'keyword'=>$keyword,"movieid"=>$movie->id,"picUrl"=>urlencode($image),'target'=>'movie_detail'));
					echo "<div class='blog' onclick=link(event,'".$url."') link=$url><h2 style='font-size:22px;'>$title</h2><br/><img width=297 height=444 src=$image></img><br/></div>";
				}
			}
			if(empty($movies->count))
				echo "NO_MORE";
		}
	}   