<?php
	require_once("DouOAuth.php");
	require_once("Dou_action_hook.php");
	require_once("DouError.php");
	require_once("DouScope.php");
	require_once("DouBuilder.php");
	
	//由关键字或标签获取图书信息
	function Dou_searchBooks($keyword,$tag="",$offset=0,$count=5)
	{
		$request = HTTPS."://api.douban.com/v2/book/search?"."q=$keyword&"."tag=$tag&"."start=$offset&"."count=$count";
		$result = file_get_contents($request);
		$obj = json_decode($result);
		return $obj;
	}
	//由id获取书籍详细信息
	function Dou_searchBook($id)
	{
		$request = HTTPS."://api.douban.com/v2/book/".$id;
		$result = file_get_contents($request);
		$obj = json_decode($result);
		return $obj;
	}
	
	//获取影片简略信息
	function Dou_searchMovies_list($keyword,$tag="",$offset=0,$count=MAXCOUNT)
	{
		$request = HTTPS."://api.douban.com/v2/movie/search?"."q=$keyword&"."tag=$tag&"."start=$offset&"."count=$count";
		$result = file_get_contents($request);
		$obj = json_decode($result);
		return $obj;
	}
	
	//由id获取影片详细信息
	function Dou_searchMovie($id)
	{
		$request = HTTPS."://api.douban.com/v2/movie/".$id;
		$result = file_get_contents($request);
		$obj = json_decode($result);
		return $obj;
	}
	
	//由id获取影评 advanced
	function Dou_searchMovieReview($id)
	{
		$request = HTTPS."://api.douban.com/v2/movie/subject/".$id."/reviews";
		$result = file_get_contents($request);
		$obj = json_decode($result);
		return $obj;
	}
	
	//批量获取影片详细信息
	function Dou_searchMovies($keyword,$tag="",$offset=0,$count=MAXCOUNT)
	{
		$request = HTTPS."://api.douban.com/v2/movie/search?"."q=$keyword&"."tag=$tag&"."start=$offset&"."count=$count";
		$result = file_get_contents($request);
		$obj = json_decode($result);
		$total = $obj->total;
		$json = '{"total":'.$total.',"subjects":[';
		for($i=0;$i<$total-1;$i++)
		{
			$request2 = HTTPS."://api.douban.com/v2/movie/subject/".$obj->subjects[$i]->id;
			$result2 = file_get_contents($request2);
			$json .= $result2.",";
		}
		$request2 = HTTPS."://api.douban.com/v2/movie/subject/".$obj->subjects[$total-1]->id;
		$result2 = file_get_contents($request2);
		$json .= $result2;
		$json .= "]}";
		$obj2 = json_decode($json);
		return $obj2;
	}