<?php
	function D($modelName)
	{
		global $db;
		$modelDir = "MC/Model";
		$classFName = $modelName."Model.class.php";
		$className = $modelName."Model";
		require_once($modelDir."/".$classFName);
		return new $className($db);
	}