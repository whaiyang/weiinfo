<?php
	
	function setURL($params,$action=null,$sub_action=null)
	{
		$str = "";
		$i = 0;
		if($action==null)
			$url = 'http://'.DOMAIN.PHPSELF.'?action=weixin&sub_action=WEB&';
		else
			$url = 'http://'.DOMAIN.PHPSELF.'?action='.$action.'&sub_action='.$sub_action.'&';
		foreach($params as $key=>$value)
		{
			$str .= $key.'='.$value;
			if($i<count($params)-1)
				$str .= "&";
			$i++;
		}
		return $url.$str;
	}

	function setController($controller,$func="index",$weiid="")
	{
		return setURL(array("controller"=>$controller,"func"=>$func,"weiid"=>$weiid));
	}
	
	function getPicUrl($name)
	{
		if(APP_NAME=="")
			return "http://".URLDIR.'Public/image/'.$name;
		else
			return "http://".URLDIR.APP_NAME.'/Public/image/'.$name;
	}